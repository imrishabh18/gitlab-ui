export const badgeTypes = ['experiment', 'beta'];
export const badgeTypeValidator = (value) => badgeTypes.includes(value);
